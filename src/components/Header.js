import React from 'react';

function Header({ titulo, edad }) {

    let mensaje
    if (edad >= 18) {
        mensaje = 'Soy mayor de edad'
    } else {
        mensaje = 'Soy menor'
    }

    return (
        <header>
            <h1>{titulo}</h1>
            <h3>{edad >= 18 ? 'Soy mayor' : 'Soy menor'}</h3>
        </header>
    )
}

export default Header