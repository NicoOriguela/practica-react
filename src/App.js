import React from 'react';
import Header from './components/Header'
import Footer from './components/Footer'
import './App.css';

function App() {
  //acá solo va codigo de javascript puro, hasta antes del return
  let anio = new Date().getFullYear()
  let titulo = "Hola mundo"
  let edad = 18

  

  return (
    <div className="App">
      <Header 
        titulo = {titulo}
        
      />
      <Footer 
        anio = {anio}
      />
    </div>
  );
}

export default App;
